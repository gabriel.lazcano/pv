package main;

import views.*;
import models.*;
import controllers.*;

public class Main {

    private static ModelMain modelMain;

    private static ViewLogin viewLogin;
    private static UsuarioDAO usuarioDao;
    private static ControllerLogin controllerLogin;
    
    private static ViewProductos viewProductos;
    private static ViewVentas viewVentas;
    private static ViewVentasDetalle viewVentasDetalle;
    
    private static ViewAdministrador viewAdministrador;
    private static ControllerAdministrador controllerAdministrador;
    
    private static ViewProductosNuevo viewProductosNuevo;
    private static ViewProductosModificar viewProductosModificar;
    private static ControllerProductos controllerProductos;
    
    private static ControllerVentas controllerVentas;
    private static ControllerVentasDetalle controllerVentasPruebas;
    
    

    public static void main(String[] args) {

        modelMain = new ModelMain();
        viewProductos = new ViewProductos();
        viewVentas = new ViewVentas();

        viewLogin = new ViewLogin();
        usuarioDao = new UsuarioDAO();
        viewAdministrador = new ViewAdministrador();
        
        viewProductosNuevo = new ViewProductosNuevo();
        viewProductosModificar = new ViewProductosModificar();
        
        viewVentasDetalle = new ViewVentasDetalle();
        
        controllerLogin = new ControllerLogin(viewLogin, usuarioDao, viewAdministrador);
              
        controllerAdministrador = new ControllerAdministrador(viewAdministrador, viewVentas, viewProductos);
        
        controllerProductos = new ControllerProductos(viewProductos, viewProductosNuevo, viewProductosModificar);
        
        controllerVentas = new ControllerVentas(viewVentas, viewProductosNuevo, viewVentasDetalle);
        
        controllerVentasPruebas = new ControllerVentasDetalle(viewVentasDetalle, viewVentas);
        
        
    }
}
