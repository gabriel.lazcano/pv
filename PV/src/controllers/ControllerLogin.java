/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import models.Usuario;
import models.UsuarioDAO;
import views.ViewLogin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import views.*;

/**
 *
 * @author Gabriel
 */
public class ControllerLogin implements ActionListener {

    ViewLogin viewlogin;//vista
    UsuarioDAO usuarioDao;//modelo
    ViewAdministrador viewAdministrador;
    
    public ControllerLogin(ViewLogin viewlogin, UsuarioDAO usuarioDao, ViewAdministrador viewAdministrador) {
        this.viewlogin = viewlogin;
        this.usuarioDao = usuarioDao;
        this.viewAdministrador = viewAdministrador;
        this.viewlogin.btnSiguiente.addActionListener(this);
        this.viewlogin.btnSalir.addActionListener(this);
        initView();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == viewlogin.btnSiguiente) {
            //validar();
            //validarContrasena();
            viewAdministrador.setVisible(true);
            viewlogin.dispose();
        }
        if (e.getSource() == viewlogin.btnSalir) {
            System.exit(0);
        }
    }

    public void validarContrasena() {
        String usuario = viewlogin.txfUsuario.getText();
        String pass = viewlogin.pwfContrasena.getText();

        ArrayList<Usuario> list;
        list = usuarioDao.validarContrasena(usuario, pass);
        if (list.size() > 0) {
            JOptionPane.showMessageDialog(null, "Bienvenido", "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            //viewMain.setVisible(true);
            viewlogin.dispose();
        } else {
            JOptionPane.showMessageDialog(null, "Acceso denegado", "Error", JOptionPane.ERROR_MESSAGE);
            limpiar();
        }
    }

    void limpiar() {
        viewlogin.txfUsuario.setText("");
        viewlogin.pwfContrasena.setText("");
    }

    boolean validar() {
        boolean ok = false;
        if (!viewlogin.txfUsuario.getText().isEmpty() & !viewlogin.pwfContrasena.getText().isEmpty()) {
            ok = true;
        } else {
            JOptionPane.showMessageDialog(null, "Existen campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        }
        return ok;
    }

    private void initView() {
        this.viewlogin.setVisible(true);
        this.viewlogin.setLocationRelativeTo(null);
    }
}
