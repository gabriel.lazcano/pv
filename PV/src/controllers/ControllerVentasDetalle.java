/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import views.ViewVentasDetalle;

import views.ViewVentas;

/**
 *
 * @author gabriel
 */
public class ControllerVentasDetalle implements ActionListener {
    ViewVentasDetalle viewVentasDetalle;
    ViewVentas viewVentas;

    public ControllerVentasDetalle( ViewVentasDetalle viewVentasDetalle, ViewVentas viewVentas) {
        this.viewVentasDetalle = viewVentasDetalle;
        this.viewVentas = viewVentas;
        this.viewVentas.btnBorarArticulo.addActionListener(this);
        this.viewVentasDetalle.tabs.add(new JScrollPane(createTabbedPanel()), "Cliente 1");//Inicializa la tabla de ventas

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == viewVentas.btnBorarArticulo) {
            int ntab  = this.viewVentasDetalle.tabs.getTabCount();//Obtiene la cantidad de pestañas
            String st = JOptionPane.showInputDialog(null, "Ingresa el nombre del cliente"); //Agregar el nombre de la pestaña
            this.viewVentasDetalle.tabs.add(new JScrollPane(createTabbedPanel()), st);
            this.viewVentasDetalle.tabs.setSelectedIndex(ntab);
        }

    }

    private JPanel createTabbedPanel() {
        JPanel panel = new JPanel();
        //Button btn = new Button("boton");
        
//        Vector columnas = new Vector();
//        columnas.add(" ");
//        columnas.add("Código de Barras");
//        columnas.add("Cantidad");
//        columnas.add("Descripción del Producto");
//        columnas.add("Precio de Venta");    
//        columnas.add("Importe");
//
//        Vector filas = new Vector();
//        Vector fila = new Vector();
//
//        fila.add("");
//        fila.add("");
//        fila.add("");
//        fila.add("");
//        fila.add("");
//        fila.add("");
//        filas.add(fila);
//
//        JTable tbl = new JTable(filas, columnas);
//        panel.setBackground(Color.WHITE);
//        panel.add(btn);
//        //panel.add(l);
//        panel.add(tbl);
//        panel.add(new JScrollPane(tbl));
        JTable j; 
        // Data to be displayed in the JTable 
        String[][] data = { 
            { "Kundan Kumar Jha", "4031", "CSE" }, 
            { "Anand Jha", "6014", "IT" } 
        }; 
  
        // Column Names 
        String[] columnNames = { "Name", "Roll Number", "Department" }; 
  
        // Initializing the JTable 
        j = new JTable(data, columnNames); 
        j.setBounds(8830, 40, 1200, 800); 
  
        // adding it to JScrollPane 
        JScrollPane sp = new JScrollPane(j); 
        panel.add(sp); 
        // Frame Size 
        panel.setSize(500, 200); 
        // Frame Visible = true 
        panel.setVisible(true); 
        
        
        
        return panel;
    }
}
