package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import views.ViewProductos;
import views.ViewVentas;
import views.ViewAdministrador;

public class ControllerAdministrador implements ActionListener {
    ViewProductos viewProductos;
    ViewVentas viewVentas;
    ViewAdministrador viewAdministrador;

    public ControllerAdministrador(ViewAdministrador viewAdministrador, ViewVentas viewVentas, ViewProductos viewProductos) {
        this.viewAdministrador = viewAdministrador;
        this.viewVentas = viewVentas;
        this.viewProductos = viewProductos;
        this.viewAdministrador.btnVentas.addActionListener(this);
        this.viewAdministrador.btnProductos.addActionListener(this);
        abrirVentas();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == viewAdministrador.btnVentas) {
            abrirVentas();
        }
        if (e.getSource() == viewAdministrador.btnProductos) {
            abrirProductos();
    }
    }

    public void abrirProductos(){
        this.viewAdministrador.pnlContenedor.add(viewProductos);
        viewProductos.setVisible(true);
        viewVentas.setVisible(false);
        this.viewAdministrador.pnlContenedor.revalidate();
        this.viewAdministrador.pnlContenedor.repaint();
    }
    public void abrirVentas(){
        this.viewAdministrador.pnlContenedor.add(viewVentas);
        viewVentas.setVisible(true);
        viewProductos.setVisible(false);
        this.viewAdministrador.pnlContenedor.revalidate();
        this.viewAdministrador.pnlContenedor.repaint();
    }
}