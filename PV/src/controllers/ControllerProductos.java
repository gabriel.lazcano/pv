/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import views.ViewProductos;
import views.ViewProductosNuevo;
import views.ViewProductosModificar;

/**
 *
 * @author gabriel
 */
public class ControllerProductos implements ActionListener {
    ViewProductos viewProductos;
    ViewProductosNuevo viewProductosNuevo;
    ViewProductosModificar viewProductosModificar;

    public ControllerProductos(ViewProductos viewProductos, ViewProductosNuevo viewProductosNuevo, ViewProductosModificar viewProductosModificar){
        this.viewProductos = viewProductos;
        this.viewProductosNuevo = viewProductosNuevo;
        this.viewProductosModificar = viewProductosModificar;
        this.viewProductos.btnNuevo.addActionListener(this);
        this.viewProductos.btnModificar.addActionListener(this);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == viewProductos.btnNuevo) {
            abrirNuevoProducto();
        }
        if (e.getSource() == viewProductos.btnModificar) {
            abrirModificarProducto();
        }

    }

    public void abrirNuevoProducto() {
        this.viewProductos.pnlContenedorProductos.add(viewProductosNuevo);
        viewProductosNuevo.setVisible(true);
        viewProductosModificar.setVisible(false);
        this.viewProductos.pnlContenedorProductos.revalidate();
        this.viewProductos.pnlContenedorProductos.repaint();
    }

    public void abrirModificarProducto() {
        this.viewProductos.pnlContenedorProductos.add(viewProductosModificar);
        viewProductosModificar.setVisible(true);
        viewProductosNuevo.setVisible(false);
        this.viewProductos.pnlContenedorProductos.revalidate();
        this.viewProductos.pnlContenedorProductos.repaint();
    }
}
