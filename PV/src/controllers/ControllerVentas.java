/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import views.ViewVentas;
import views.ViewProductosNuevo;
import views.ViewVentasDetalle;

/**
 *
 * @author gabriel
 */
public class ControllerVentas implements ActionListener {

    ViewVentas viewVentas;
    ViewProductosNuevo viewProductosNuevo;
    ViewVentasDetalle viewVentasDetalle;

    public ControllerVentas(ViewVentas viewVentas, ViewProductosNuevo viewProductosNuevo, ViewVentasDetalle viewVentasDetalle) {
        this.viewVentas = viewVentas;
        this.viewProductosNuevo = viewProductosNuevo;
        this.viewVentasDetalle = viewVentasDetalle;
        initViewVentasDetalle();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //if (e.getSource() == viewVentas.btnVentaPendiente) {
          //  abrirVentaPendiente();
        //}

    }
    
    private void initViewVentasDetalle() {
        this.viewVentas.pnlContenedorProductos.add(viewVentasDetalle);
    }
}
